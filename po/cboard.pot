# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the cboard package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: cboard 0.7.5\n"
"Report-Msgid-Bugs-To: bjk@luxsci.net\n"
"POT-Creation-Date: 2018-09-28 09:54-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/cboard.c:286
msgid "none"
msgstr ""

#: src/cboard.c:347
msgid "NAG Menu Keys"
msgstr ""

#: src/cboard.c:348
msgid ""
"    UP/DOWN - previous/next menu item\n"
"   HOME/END - first/last menu item\n"
"  PGDN/PGUP - next/previous page\n"
"  a-zA-Z0-9 - jump to item\n"
"      SPACE - toggle selected item\n"
"     CTRL-X - quit with changes"
msgstr ""

#: src/cboard.c:422
msgid "Numeric Annotation Glyphs"
msgstr ""

#: src/cboard.c:435
msgid "Viewing NAG for"
msgstr ""

#: src/cboard.c:466 src/cboard.c:481
msgid "Viewing Annotation for"
msgstr ""

#: src/cboard.c:486 src/cboard.c:491
msgid "Any other key to continue"
msgstr ""

#: src/cboard.c:487 src/cboard.c:492
msgid "Press 'n' to view NAG"
msgstr ""

#: src/cboard.c:493
msgid "No comment text for this move"
msgstr ""

#: src/cboard.c:513 src/cboard.c:4750
msgid "Not a regular file"
msgstr ""

#: src/cboard.c:559 src/cboard.c:633 src/cboard.c:5917
msgid "Save game failed."
msgstr ""

#: src/cboard.c:561 src/cboard.c:635
msgid "Game saved."
msgstr ""

#: src/cboard.c:610 src/cboard.c:6038
msgid "Not a directory."
msgstr ""

#: src/cboard.c:624 src/cboard.c:4267 src/cboard.c:5092
msgid "What would you like to do?"
msgstr ""

#: src/cboard.c:627
msgid "File exists:"
msgstr ""

#: src/cboard.c:1474 src/cboard.c:1478
msgid "Ambiguous move"
msgstr ""

#: src/cboard.c:1474 src/cboard.c:1478
msgid "Invalid move"
msgstr ""

#: src/cboard.c:1583 src/cboard.c:1834 src/menu.c:147
msgid "of"
msgstr ""

#: src/cboard.c:1584
msgid " (ply)"
msgstr ""

#: src/cboard.c:1586 src/cboard.c:1595 src/cboard.c:1626 src/cboard.c:1831
msgid "not available"
msgstr ""

#: src/cboard.c:1589
msgid "Move:"
msgstr ""

#: src/cboard.c:1595 src/cboard.c:1626
msgid "empty"
msgstr ""

#: src/cboard.c:1600 src/cboard.c:1631
msgid " (Annotated"
msgstr ""

#: src/cboard.c:1620
msgid "Next:"
msgstr ""

#: src/cboard.c:1620
msgid "Next move:"
msgstr ""

#: src/cboard.c:1651
msgid "Prev.:"
msgstr ""

#: src/cboard.c:1651
msgid "Prev move:"
msgstr ""

#: src/cboard.c:1733 src/cboard.c:1734
msgid "abcdefgh"
msgstr ""

#: src/cboard.c:1742
msgid "Select Pawn Promotion Piece"
msgstr ""

#: src/cboard.c:1742
msgid "R/N/B/Q"
msgstr ""

#: src/cboard.c:1745
msgid "R = Rook, N = Knight, B = Bishop, Q = Queen"
msgstr ""

#: src/cboard.c:1832
msgid "File:"
msgstr ""

#: src/cboard.c:1835
msgid "Game:"
msgstr ""

#: src/cboard.c:1895
msgid "Flags:"
msgstr ""

#: src/cboard.c:1901
msgid "move history"
msgstr ""

#: src/cboard.c:1904
msgid "edit"
msgstr ""

#: src/cboard.c:1907
msgid "play"
msgstr ""

#: src/cboard.c:1910 src/cboard.c:1948
msgid "(empty value)"
msgstr ""

#: src/cboard.c:1914
msgid "Mode:"
msgstr ""

#: src/cboard.c:1919
msgid " (human/human)"
msgstr ""

#: src/cboard.c:1921
msgid " (engine/engine)"
msgstr ""

#: src/cboard.c:1924
msgid " (engine/human)"
msgstr ""

#: src/cboard.c:1924
msgid " (human/engine)"
msgstr ""

#: src/cboard.c:1936
msgid "pondering..."
msgstr ""

#: src/cboard.c:1939
msgid "ready"
msgstr ""

#: src/cboard.c:1942
msgid "initializing..."
msgstr ""

#: src/cboard.c:1945 src/cboard.c:1953
msgid "offline"
msgstr ""

#: src/cboard.c:1955
msgid "Engine:"
msgstr ""

#: src/cboard.c:1960
msgid "Turn:"
msgstr ""

#: src/cboard.c:1961 src/cboard.c:1963
msgid "white"
msgstr ""

#: src/cboard.c:1961 src/cboard.c:1971
msgid "black"
msgstr ""

#: src/cboard.c:1979
msgid "Total:"
msgstr ""

#: src/cboard.c:1989 src/cboard.c:5524 src/input.c:128
#, c-format
msgid "Type %ls for help"
msgstr ""

#: src/cboard.c:2098 src/cboard.c:5208
msgid "Engine IO Window"
msgstr ""

#: src/cboard.c:2236
msgid "Error Matching Regular Expression"
msgstr ""

#: src/cboard.c:2265 src/cboard.c:4604 src/cboard.c:4624
msgid "Cannot delete last game."
msgstr ""

#: src/cboard.c:2297 src/cboard.c:2315 src/cboard.c:3738
msgid "Error Compiling Regular Expression"
msgstr ""

#: src/cboard.c:2466
msgid "Game Status"
msgstr ""

#: src/cboard.c:2469
msgid "Roster Tags"
msgstr ""

#: src/cboard.c:2472
msgid "Move History"
msgstr ""

#: src/cboard.c:2719
msgid "Maximum number of time controls reached"
msgstr ""

#: src/cboard.c:2780
msgid "Invalid clock specification"
msgstr ""

#: src/cboard.c:2888
msgid "Set Clock"
msgstr ""

#: src/cboard.c:2890
msgid ""
"Format: [W | B] [+]T[+I] | ++I | M/T [M/T [...] [SD/T]] [+I]\n"
"T = time (hms), I = increment, M = moves per, SD = sudden death\n"
"e.g., 30m or 4m+12s or 35/90m SD/30m"
msgstr ""

#: src/cboard.c:2949
msgid "Engine Command"
msgstr ""

#: src/cboard.c:2980
msgid "Press 'g' to start the game"
msgstr ""

#: src/cboard.c:2986
msgid ""
"You may only switch sides at the start of the \n"
"game. Press ^K or ^N to begin a new game."
msgstr ""

#: src/cboard.c:3224
#, c-format
msgid ""
"It is not your turn to move. You may switch playing sides by pressing \"%lc"
"\"."
msgstr ""

#: src/cboard.c:3234
msgid "It is not your turn to move. You can switch sides "
msgstr ""

#: src/cboard.c:3349
msgid "more help"
msgstr ""

#: src/cboard.c:3453
msgid "Global Game Keys (* = can take a repeat count)"
msgstr ""

#: src/cboard.c:3492
msgid "Command Key Index"
msgstr ""

#: src/cboard.c:3493
msgid "p/h/e/g or any other key to quit"
msgstr ""

#: src/cboard.c:3495
msgid ""
" p - play mode keys\n"
" h - history mode keys\n"
" e - board edit mode keys\n"
" g - global game keys"
msgstr ""

#: src/cboard.c:3507
msgid "Play Mode Keys (* = can take a repeat count)"
msgstr ""

#: src/cboard.c:3637
msgid "Insert Piece"
msgstr ""

#: src/cboard.c:3638
msgid "P=pawn, R=rook, N=knight, B=bishop, Q=queen, K=king"
msgstr ""

#: src/cboard.c:3641
msgid ""
"Type the piece letter to insert. Lowercase for a black piece, uppercase for "
"a white piece."
msgstr ""

#: src/cboard.c:3664
msgid "Edit Mode Keys (* = can take a repeat count)"
msgstr ""

#: src/cboard.c:4012
msgid "History Menu Help"
msgstr ""

#: src/cboard.c:4013
msgid ""
"    UP/DOWN - previous/next menu item\n"
"   HOME/END - first/last menu item\n"
"  PGDN/PGUP - next/previous page\n"
"  a-zA-Z0-9 - jump to item\n"
"     CTRL-a - annotate the selected move\n"
"      ENTER - view annotation\n"
"     CTRL-d - toggle board details\n"
"   ESCAPE/M - return to move history"
msgstr ""

#: src/cboard.c:4029 src/cboard.c:4073
msgid "Editing Annotation for"
msgstr ""

#: src/cboard.c:4035 src/cboard.c:4080
msgid "Type CTRL-t to edit NAG"
msgstr ""

#: src/cboard.c:4158
msgid "Move History Tree"
msgstr ""

#: src/cboard.c:4270
#, c-format
msgid ""
"The current move is not the final move of this round. Press \"%ls\" to "
"resume a game from the current move and discard future moves or any other "
"key to cancel."
msgstr ""

#: src/cboard.c:4311
msgid "History Mode Keys (* = can take a repeat count)"
msgstr ""

#: src/cboard.c:4333
msgid "Find Move Text Expression"
msgstr ""

#: src/cboard.c:4394
msgid "Jump to Move Number"
msgstr ""

#: src/cboard.c:4480
#, c-format
msgid "Loading... %i%% (%i games)"
msgstr ""

#: src/cboard.c:4619
msgid "Delete the current game?"
msgstr ""

#: src/cboard.c:4628
msgid "Delete all games marked for deletion?"
msgstr ""

#: src/cboard.c:4635 src/cboard.c:5174 src/cboard.c:5195
msgid "[ Yes or No ]"
msgstr ""

#: src/cboard.c:4652
msgid "No matches found"
msgstr ""

#: src/cboard.c:4870
msgid "Save Game Filename"
msgstr ""

#: src/cboard.c:4871 src/cboard.c:5083
msgid "Type TAB for file browser"
msgstr ""

#: src/cboard.c:4887
msgid "Save game aborted."
msgstr ""

#: src/cboard.c:4897
msgid "ABOUT"
msgstr ""

#: src/cboard.c:4898
#, c-format
msgid ""
"%s\n"
"Using %s with %i colors and %i color pairs\n"
"%s\n"
"%s"
msgstr ""

#: src/cboard.c:4963
msgid "Find Game by Tag Expression"
msgstr ""

#: src/cboard.c:4964
msgid "[name expression:]value expression"
msgstr ""

#: src/cboard.c:5004
msgid "Jump to Game Number"
msgstr ""

#: src/cboard.c:5082
msgid "Load Filename"
msgstr ""

#: src/cboard.c:5095
#, c-format
msgid ""
"There is more than one game loaded. Press \"%ls\" to save the current game, "
"\"%ls\" to save all games or any other key to cancel."
msgstr ""

#: src/cboard.c:5176
msgid "Really start a new game from scratch?"
msgstr ""

#: src/cboard.c:5196
msgid "Want to Quit?"
msgstr ""

#: src/cboard.c:5271
#, c-format
msgid "Repeat %i"
msgstr ""

#: src/cboard.c:5390
msgid "FEN parse error."
msgstr ""

#: src/cboard.c:5424
msgid "PERL Subroutine Filter"
msgstr ""

#: src/cboard.c:5823
msgid ""
"Usage: cboard [-hvCD] [-u [N]] [-p [-VtRSE] <file>]\n"
"  -D  Dump libchess debugging info to \"libchess.debug\" (stderr)\n"
msgstr ""

#: src/cboard.c:5826
msgid "Usage: cboard [-hvC] [-u [N]] [-p [-VtRSE] <file>]\n"
msgstr ""

#: src/cboard.c:5828
msgid ""
"  -p  Load PGN file.\n"
"  -V  Validate a game file.\n"
"  -S  Validate and output a PGN formatted game.\n"
"  -R  Like -S but write a reduced PGN formatted game.\n"
"  -t  Also write custom PGN tags from config file.\n"
"  -E  Stop processing on file parsing error (overrides config).\n"
"  -C  Enable strict castling (overrides config).\n"
"  -u  Enable/disable UTF-8 pieces (1=enable, 0=disable, overrides config).\n"
"  -v  Version information.\n"
"  -h  This help text.\n"
msgstr ""

#: src/cboard.c:5940
msgid "Broken pipe. Quitting."
msgstr ""

#: src/cboard.c:5973
#, c-format
msgid "Loading... %i%% (%i games)%c"
msgstr ""

#: src/cboard.c:6165
msgid "y"
msgstr ""

#: src/cboard.c:6166 src/cboard.c:6170
msgid "a"
msgstr ""

#: src/cboard.c:6167
msgid "o"
msgstr ""

#: src/cboard.c:6168
msgid "r"
msgstr ""

#: src/cboard.c:6169
msgid "c"
msgstr ""

#: src/cboard.c:6171
msgid "Event"
msgstr ""

#: src/cboard.c:6172
msgid "Site"
msgstr ""

#: src/cboard.c:6173
msgid "Date"
msgstr ""

#: src/cboard.c:6174
msgid "Round"
msgstr ""

#: src/cboard.c:6175
msgid "White"
msgstr ""

#: src/cboard.c:6176
msgid "Black"
msgstr ""

#: src/cboard.c:6177
msgid "Result"
msgstr ""

#: src/cboard.c:6190
msgid "Could not initialize curses."
msgstr ""

#: src/cboard.c:6197
msgid "Need at least an 74x23 terminal."
msgstr ""

#: src/common.h:92
msgid "[ press any key to continue ]"
msgstr ""

#: src/common.h:93
msgid "[ press any key to continue (UP/DN to scroll) ]"
msgstr ""

#: src/common.h:94
msgid "[ ERROR ]"
msgstr ""

#: src/engine.c:118
msgid "Could not write to engine. Process no longer exists."
msgstr ""

#: src/engine.c:134
#, c-format
msgid "short write() count to engine. Expected %i, got %i."
msgstr ""

#: src/filebrowser.c:229
msgid "File Browser Keys"
msgstr ""

#: src/filebrowser.c:230
msgid ""
"    UP/DOWN - previous/next menu item\n"
"   HOME/END - first/last menu item\n"
"  PGDN/PGUP - next/previous page\n"
"  a-zA-Z0-9 - jump to item\n"
"      ENTER - select item\n"
"          ~ - change to home directory\n"
"     CTRL-e - change filename expression\n"
"     ESCAPE - abort"
msgstr ""

#: src/filebrowser.c:332
msgid "Filename Expression"
msgstr ""

#: src/input.c:157
msgid "Line Editing Keys"
msgstr ""

#: src/input.c:159
msgid ""
"UP/DOWN/LEFT/RIGHT - position cursor (multiline)\n"
"         UP/CTRL-P - previous input history\n"
"       DOWN/CTRL-N - next input history\n"
"       HOME/CTRL-A - move cursor to the beginning of line\n"
"        END/CTRL-E - move cursor to the end of line\n"
"          CTRL-B/W - move cursor to previous/next word\n"
"            CTRL-X - delete word under cursor\n"
"            CTRL-K - delete from cursor to end of line\n"
"            CTRL-U - clear entire input field\n"
"         BACKSPACE - delete previous character\n"
"            ESCAPE - quit without changes\n"
"             ENTER - quit with changes"
msgstr ""

#: src/menu.c:146
#, c-format
msgid "Item %i %s %i  Type %ls for help"
msgstr ""

#: src/menu.c:174
msgid "empty value"
msgstr ""

#: src/misc.c:189 src/misc.c:193
msgid "..."
msgstr ""

#: src/rcfile.c:69
#, c-format
msgid "%s(%i): invalid attribute \"%s\""
msgstr ""

#: src/rcfile.c:98
#, c-format
msgid "%s(%i): invalid color \"%s\""
msgstr ""

#: src/rcfile.c:114
#, c-format
msgid "%s(%i): parse error"
msgstr ""

#: src/rcfile.c:142 src/rcfile.c:898 src/rcfile.c:1069
#, c-format
msgid "%s(%i): invalid value \"%s\""
msgstr ""

#: src/rcfile.c:185
msgid "Escape"
msgstr ""

#: src/rcfile.c:188
msgid "Enter"
msgstr ""

#: src/rcfile.c:211
msgid "PgUp"
msgstr ""

#: src/rcfile.c:213
msgid "PgDown"
msgstr ""

#: src/rcfile.c:220
msgid "Space"
msgstr ""

#: src/rcfile.c:254
msgid "history jump next"
msgstr ""

#: src/rcfile.c:256
msgid "history jump previous"
msgstr ""

#: src/rcfile.c:257
msgid "next move"
msgstr ""

#: src/rcfile.c:260
msgid "previous move"
msgstr ""

#: src/rcfile.c:262
msgid "toggle half move (ply) stepping"
msgstr ""

#: src/rcfile.c:264
msgid "rotate board"
msgstr ""

#: src/rcfile.c:266
msgid "jump to move number"
msgstr ""

#: src/rcfile.c:268
msgid "new move text expression"
msgstr ""

#: src/rcfile.c:270
msgid "find next move text expression"
msgstr ""

#: src/rcfile.c:272
msgid "find previous move text expression"
msgstr ""

#: src/rcfile.c:274
msgid "annotate the previous move"
msgstr ""

#: src/rcfile.c:276
msgid "next variation of the previous move"
msgstr ""

#: src/rcfile.c:278
msgid "previous variation of the previous move"
msgstr ""

#: src/rcfile.c:280
msgid "move history tree"
msgstr ""

#: src/rcfile.c:282
msgid "exit history mode"
msgstr ""

#: src/rcfile.c:285 src/rcfile.c:301
msgid "select piece for movement"
msgstr ""

#: src/rcfile.c:287 src/rcfile.c:303
msgid "commit selected piece"
msgstr ""

#: src/rcfile.c:289 src/rcfile.c:305
msgid "cancel selected piece"
msgstr ""

#: src/rcfile.c:291
msgid "remove the piece under the cursor"
msgstr ""

#: src/rcfile.c:292
msgid "insert piece"
msgstr ""

#: src/rcfile.c:294
msgid "toggle castling availability"
msgstr ""

#: src/rcfile.c:296
msgid "toggle enpassant square"
msgstr ""

#: src/rcfile.c:297
msgid "toggle turn"
msgstr ""

#: src/rcfile.c:298
msgid "exit edit mode"
msgstr ""

#: src/rcfile.c:306
msgid "set clock"
msgstr ""

#: src/rcfile.c:308
msgid "undo previous move"
msgstr ""

#: src/rcfile.c:310
msgid "force the chess engine to make the next move"
msgstr ""

#: src/rcfile.c:312
msgid "send a command to the chess engine"
msgstr ""

#: src/rcfile.c:314
msgid "toggle engine/human play"
msgstr ""

#: src/rcfile.c:316
msgid "toggle engine/engine play"
msgstr ""

#: src/rcfile.c:318
msgid "toggle human/human play"
msgstr ""

#: src/rcfile.c:320
msgid "toggle pausing of this game"
msgstr ""

#: src/rcfile.c:322
msgid "enter history mode"
msgstr ""

#: src/rcfile.c:323
msgid "enter edit mode"
msgstr ""

#: src/rcfile.c:326
msgid "toggle strict castling"
msgstr ""

#: src/rcfile.c:329
msgid "edit roster tags"
msgstr ""

#: src/rcfile.c:331
msgid "view roster tags"
msgstr ""

#: src/rcfile.c:333
msgid "new find game expression"
msgstr ""

#: src/rcfile.c:335
msgid "find next game"
msgstr ""

#: src/rcfile.c:337
msgid "find previous game"
msgstr ""

#: src/rcfile.c:339
msgid "new game or round"
msgstr ""

#: src/rcfile.c:341
msgid "new game from scratch"
msgstr ""

#: src/rcfile.c:343
msgid "copy current game"
msgstr ""

#: src/rcfile.c:345
msgid "copy current game as FEN tag"
msgstr ""

#: src/rcfile.c:346
msgid "next game"
msgstr ""

#: src/rcfile.c:347
msgid "previous game"
msgstr ""

#: src/rcfile.c:349
msgid "jump to game"
msgstr ""

#: src/rcfile.c:352
msgid "toggle delete flag"
msgstr ""

#: src/rcfile.c:354
msgid "delete the current or flagged games"
msgstr ""

#: src/rcfile.c:356
msgid "load a PGN file"
msgstr ""

#: src/rcfile.c:357
msgid "save game"
msgstr ""

#: src/rcfile.c:359
msgid "toggle board details"
msgstr ""

#: src/rcfile.c:361
msgid "toggle chess engine IO window"
msgstr ""

#: src/rcfile.c:364
msgid "Call PERL subroutine"
msgstr ""

#: src/rcfile.c:367
msgid "version information"
msgstr ""

#: src/rcfile.c:369
msgid "redraw the screen"
msgstr ""

#: src/rcfile.c:371
msgid "quit"
msgstr ""

#: src/rcfile.c:444
msgid "no description"
msgstr ""

#: src/rcfile.c:545 src/rcfile.c:557 src/rcfile.c:563
#, c-format
msgid "%s(%i): parse error \"%ls\""
msgstr ""

#: src/rcfile.c:588 src/rcfile.c:739
#, c-format
msgid "%s(%i): too few arguments"
msgstr ""

#: src/rcfile.c:599 src/rcfile.c:750
#, c-format
msgid "%s(%i): invalid game mode \"%s\""
msgstr ""

#: src/rcfile.c:611
#, c-format
msgid "%s(%i): key \"%ls\" conflicts with a global key"
msgstr ""

#: src/rcfile.c:626
#, c-format
msgid "%s(%i): invalid command \"%s\""
msgstr ""

#: src/rcfile.c:682
#, c-format
msgid "%s(%i): parse error\n"
msgstr ""

#: src/rcfile.c:802
#, c-format
msgid ""
"%s(%i): macro definition in mode \"%s\" conflicts with existing key in mode "
"\"%s\" (command=%ls)"
msgstr ""

#: src/rcfile.c:857
#, c-format
msgid "%s(%i): parse error %i"
msgstr ""

#: src/rcfile.c:869 src/rcfile.c:889 src/rcfile.c:954
#, c-format
msgid "%s(%i): value is not an integer"
msgstr ""

#: src/rcfile.c:913
#, c-format
msgid "%s(%i): token names must match 0-9A-Za-z_."
msgstr ""

#: src/rcfile.c:960
#, c-format
msgid "%s(%i): invalid value"
msgstr ""

#: src/rcfile.c:1087
#, c-format
msgid "%s(%i): invalid parameter \"%s\""
msgstr ""

#: src/tags.c:125
msgid "Country Code Keys"
msgstr ""

#: src/tags.c:126
msgid ""
"    UP/DOWN - previous/next menu item\n"
"   HOME/END - first/last menu item\n"
"  PGDN/PGUP - next/previous page\n"
"  a-zA-Z0-9 - jump to item\n"
"      ENTER - select item\n"
"     ESCAPE - cancel"
msgstr ""

#: src/tags.c:184
msgid "Country Codes"
msgstr ""

#: src/tags.c:256
msgid "Tag edit aborted."
msgstr ""

#: src/tags.c:330
msgid "Type CTRL-t for country codes"
msgstr ""

#: src/tags.c:366 src/tags.c:402
msgid "Editing Tag"
msgstr ""

#: src/tags.c:416
msgid "New Tag Name"
msgstr ""

#: src/tags.c:430
msgid "Cannot remove the Seven Tag Roster"
msgstr ""

#: src/tags.c:482
msgid "Tag Editing Keys"
msgstr ""

#: src/tags.c:483
msgid ""
"    UP/DOWN - previous/next menu item\n"
"   HOME/END - first/last menu item\n"
"  PGDN/PGUP - next/previous page\n"
"  a-zA-Z0-9 - jump to item\n"
"      ENTER - edit select item\n"
"     CTRL-a - add an entry\n"
"     CTRL-f - add FEN tag from current position\n"
"     CTRL-r - remove selected entry\n"
"     CTRL-t - add custom tags\n"
"     CTRL-x - quit with changes\n"
"     ESCAPE - quit without changes"
msgstr ""

#: src/tags.c:499
msgid "Tag Viewing Keys"
msgstr ""

#: src/tags.c:500
msgid ""
"    UP/DOWN - previous/next menu item\n"
"   HOME/END - first/last menu item\n"
"  PGDN/PGUP - next/previous page\n"
"  a-zA-Z0-9 - jump to item\n"
"      ENTER - view selected item\n"
"     ESCAPE - cancel"
msgstr ""

#: src/tags.c:520
msgid "Viewing Tag"
msgstr ""

#: src/tags.c:596
msgid "Editing Roster Tags"
msgstr ""

#: src/tags.c:597
msgid "Viewing Roster Tags"
msgstr ""
